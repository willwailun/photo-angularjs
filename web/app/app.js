var app = angular.module('myApp', [
    'ngRoute',
    'myApp.filters',
    'myApp.home'
]).config(['$routeProvider', ($routeProvider) => {
    $routeProvider.otherwise({redirectTo: '/'});
}]).run(['$rootScope', '$route', ($rootScope, $route) => {
    $rootScope.$on('$routeChangeSuccess', () => {
        document.title = $route.current.title;
    });
}]);