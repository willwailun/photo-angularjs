app.directive('photoModal', function () {
    return {
        restrict: 'E',
        templateUrl: 'components/photo-modal.html',
        controllerAs: 'p',
        bindToController: {
            photo: "=",
            open: "="
        },
        controller: function ($scope, $timeout) {
            /**
             * Init
             */

            /**
             * Watcher
             */
            $scope.$watch("p.open", (n) => {
                if(n) {
                    $timeout(() => {
                        angular.element(".ui.modal").modal({
                            closable: false
                        }).modal("show").modal("refresh");
                    }, 100);
                } else{
                    $timeout(() => {
                        angular.element(".ui.modal").modal("hide");
                    }, 0);
                }
            });

            /**
             * Helpers
             */

            /**
             * Actions
             */
            this.close = () => {
                this.open = false;
            };

            /**
             *  UI Component Init
             */
        }
    }
});
