'use strict';

app.directive('photoList', function () {
    return {
        restrict: 'E',
        templateUrl: 'components/photo-list.html',
        controllerAs: 'pl',
        bindToController: {
            photos: "="
        },
        controller: function controller($scope, $timeout) {
            var _this = this;

            /**
             * Init
             */
            this.selectedPhoto = null;
            this.isModalOpen = false;

            /**
             * Watchers
             */
            $scope.$watch("pl.photos", function (n) {
                if (!n.length) return;
                $timeout(function () {
                    $('.image-container').justifiedGallery({
                        rowHeight: 200,
                        maxRowHeight: '200%',
                        lastRow: 'nojustify',
                        margins: 6,
                        captionSettings: {
                            animationDuration: 200,
                            visibleOpacity: 0.7,
                            nonVisibleOpacity: 0.0
                        }
                    });
                });
            });

            /**
             * Helpers
             */

            /**
             * Actions
             */

            this.openImage = function (photo) {
                _this.selectedPhoto = photo;
                _this.isModalOpen = true;
            };

            /**
             *  UI Component Init
             */
        }
    };
});

//# sourceMappingURL=photo-list-compiled.js.map