app.directive('uploadBox', function () {
    return {
        restrict: 'E',
        templateUrl: 'components/upload-box.html',
        controllerAs: 'ub',
        bindToController: {
            handleUpload: "="
        },
        controller: function () {
            /**
             * Helpers
             */
            this.debouncedHandleUpload = _.debounce(this.handleUpload, 300);

            /**
             *  UI Component Init
             */
            let _this = this;
            let myDropzone = new Dropzone("#upload-box-id", {
                url: "http://localhost:3030/photo/upload",
                dictDefaultMessage: "<strong>DROP</strong> FILES HERE OR <strong>CLICK</strong> TO UPLOAD",
                acceptedFiles: 'image/*',
                maxFiles: '50',
                init: function() {
                    this.on("success", () => {
                        console.log("added");
                        _this.debouncedHandleUpload();
                    });
                }
            });
        }
    }
});
