'use strict';

app.directive('photoModal', function () {
    return {
        restrict: 'E',
        templateUrl: 'components/photo-modal.html',
        controllerAs: 'p',
        bindToController: {
            photo: "=",
            open: "="
        },
        controller: function controller($scope, $timeout) {
            var _this = this;

            /**
             * Init
             */

            /**
             * Watcher
             */
            $scope.$watch("p.open", function (n) {
                if (n) {
                    $timeout(function () {
                        angular.element(".ui.modal").modal({
                            closable: false
                        }).modal("show").modal("refresh");
                    }, 100);
                } else {
                    $timeout(function () {
                        angular.element(".ui.modal").modal("hide");
                    }, 0);
                }
            });

            /**
             * Helpers
             */

            /**
             * Actions
             */
            this.close = function () {
                _this.open = false;
            };

            /**
             *  UI Component Init
             */
        }
    };
});

//# sourceMappingURL=photo-modal-compiled.js.map