app.directive('photoList', function () {
    return {
        restrict: 'E',
        templateUrl: 'components/photo-list.html',
        controllerAs: 'pl',
        bindToController: {
            photos: "="
        },
        controller: function ($scope, $timeout) {
            /**
             * Init
             */
            this.selectedPhoto = null;
            this.isModalOpen = false;

            /**
             * Watchers
             */
            $scope.$watch("pl.photos", (n) => {
                if (!n.length) return;
                $timeout(() => {
                    $('.image-container').justifiedGallery({
                        rowHeight: 200,
                        maxRowHeight: '200%',
                        lastRow: 'nojustify',
                        margins: 6,
                        captionSettings: {
                            animationDuration: 200,
                            visibleOpacity: 0.7,
                            nonVisibleOpacity: 0.0
                        }
                    });
                });
            });

            /**
             * Helpers
             */

            /**
             * Actions
             */

            this.openImage = (photo) => {
                this.selectedPhoto = photo;
                this.isModalOpen = true;
            };

            /**
             *  UI Component Init
             */
        }
    }
});
