'use strict';

(function () {
    'use strict';

    angular.module('myApp.home', ['ngRoute']).config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            title: "Playlist",
            templateUrl: 'home/home.html',
            controller: 'HomeCtrl'
        });
    }]).controller('HomeCtrl', function ($scope, $http) {
        $scope.photos = [];

        $scope.getPhotos = function () {
            $http.get("http://localhost:3030/photos/list").then(function (res) {
                $scope.photos = res.data;
                console.log(res.data);
            }).catch(function (errRes) {
                console.error("error fetching the photos", errRes);
            });
        };

        /**
         * Init
         */
        $scope.getPhotos();
    });
})();

//# sourceMappingURL=home-compiled.js.map