(function () {
    'use strict';

    angular.module('myApp.home', ['ngRoute'])

        .config(['$routeProvider', ($routeProvider) => {
            $routeProvider.when('/', {
                title: "Playlist",
                templateUrl: 'home/home.html',
                controller: 'HomeCtrl'
            });
        }])

        .controller('HomeCtrl', ($scope, $http) => {
            $scope.photos = [];

            $scope.getPhotos = () => {
                $http.get("http://localhost:3030/photos/list").then((res) => {
                    $scope.photos = res.data;
                    console.log(res.data)
                }).catch((errRes) => {
                    console.error("error fetching the photos", errRes);
                });
            };

            /**
             * Init
             */
            $scope.getPhotos();
        })
}());