'use strict';

var app = angular.module('myApp', ['ngRoute', 'myApp.filters', 'myApp.home']).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({ redirectTo: '/' });
}]).run(['$rootScope', '$route', function ($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function () {
        document.title = $route.current.title;
    });
}]);

//# sourceMappingURL=app-compiled.js.map