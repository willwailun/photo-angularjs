# Frameworks/Tools Used
MongoDB, Express.js, AngularJS, Node.js, Semantic-UI

# Required
Make sure the followings are installed

* NodeJS
* MongoDB
* Imagemagick ("brew install imagemagick" or "apt-get install imagemagick")

# Start Server

```bash
cd server
npm start
```

# Start Web

```bash
cd web
npm start
```