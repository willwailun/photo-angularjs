(function () {
    "use strict";

    let mongoose = require('mongoose');
    let uuid = require('node-uuid');
    let Schema = mongoose.Schema;

    /**
     * Schema
     */
    let fileSchema = new Schema({
        name: {
            type: String,
            index: true
        },
        mimeType: String,
        size: String,
        height: Number, //in Pixels
        width: Number, //in Pixels
        thumbnailPath: String,
        path: String,
        uuid: {
            type: String,
            default: () => {
                return uuid.v4().toString();
            }
        },
        createdAt: {
            type: Date,
            default: new Date()
        },
        updatedAt: {
            type: Date,
            default: new Date()
        }
    });

    /**
     * Triggers
     */
    fileSchema.pre('save', function (next) {
        this.updatedAt = new Date();
        next();
    });

    /**
     * Export
     */
    let Files = mongoose.model('Files', fileSchema);
    module.exports = Files;
})();

//# sourceMappingURL=files-compiled.js.map