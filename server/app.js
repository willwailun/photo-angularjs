(function () {
    "use strict";
    let express = require('express');
    let Busboy = require('busboy');
    let bodyParser = require('body-parser');
    let mongoose = require('mongoose');
    let uuid = require('node-uuid');
    let Files = require("./models/files");
    let fs = require("fs");
    let _ = require("underscore");
    let im = require("imagemagick");

    const port = 3030;
    let app = express();


    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(express.static('public'));
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Cache-Control");
        next();
    });

    mongoose.connect('mongodb://127.0.0.1:27017/photo');
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', () => {
        console.log("connected to MongoDB.");
    });

    /**
     * Routes
     */
    app.post('/photo/upload', (req, res) => {
        let files = [];
        let photo = {};
        let busboy = new Busboy({
            headers: req.headers
        });
        busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
            // Build the photo object which contains all photo info
            photo.mimeType = mimetype;
            photo.encoding = encoding;
            photo.filename = filename;
            let buffers = [];
            file.on('data', (data) => {
                buffers.push(data);
            });
            file.on('end', () => {
                let buf = _(buffers).first();
                photo.data = Buffer.concat(buffers);
                photo.signature = buf.slice(0, 8).toString("hex");
                files.push(photo);
            });
        });
        busboy.on("field", (fieldname, value) => {
            req.body[fieldname] = value;
        });
        busboy.on("finish", () => {
            req.files = files;
            try {
                let file = req.files[0];
                let mimeType = getRealMimetype(file.signature);
                let newUuid = uuid.v4().toString();
                let path = `uploads/photos/${newUuid}`;
                let thumbnailPath = `uploads/thumbnails/${newUuid}`;

                // Store photo in disk first, then generate and insert the document into mongo to display on web
                fs.writeFile(`public/${path}`, file.data, 'binary', (err) => {
                    if (err) return console.error(err);
                    console.log("The file was saved");
                    im.identify(`public/${path}`, (err, features) => {
                        if (err) return console.error(err);
                        console.log("The file was identified");
                        im.crop({
                            srcData: file.data,
                            height: 300,
                            quality: 1
                        }, (err, stdout) => {
                            if (err) return console.error(err);
                            console.log("The file was resized");
                            fs.writeFile(`public/${thumbnailPath}`, stdout, 'binary', (err) => {
                                if (err) return console.error(err);
                                console.log("The thumbnail was saved");
                                Files.create({
                                    name: file.filename,
                                    mimeType: mimeType,
                                    size: features.filesize,
                                    height: features.height, //in Pixels
                                    width: features.width, //in Pixels
                                    uuid: newUuid,
                                    path: path,
                                    thumbnailPath: thumbnailPath
                                }, (err) => {
                                    if (err) return console.error(err);
                                    console.log("The file metadata was inserted");
                                })
                            });
                        });
                    });
                });
                res.writeHead(200);
            } catch (e) {
                console.error("upload error", e);
                res.writeHead(400);
            }
            res.end();
        });
        req.pipe(busboy);
    });

    app.get('/photos/list', (req, res) => {
        let s = Number(req.query.skip), l = Number(req.query.limit), orderby = req.query.orderby, asc = req.query.asc;
        let skip = (Number.isNaN(s) || s < 0) ? 0 : s, limit = (Number.isNaN(l) || l) < 0 ? 0 : l;
        if (1000 < limit) {
            console.log("The limit has reached the max number, 1000. It will now return max 1000 items instead.");
            limit = 1000;
        }
        let select = {}, sort = {}, projection = {};
        if (orderby) {
            sort[orderby] = (asc === "true") ? 1 : -1;
        }
        sort["createdAt"] = -1;
        Files.find(select, projection, {skip: skip, limit: limit, sort: sort}, (err, docs) => {
            handleResponse(res, err, docs);
        });
    });

    app.get('/photos/list/:id/details', (req, res) => {
        let id = req.params.id || null;
        Files.findById(id, (err, docs) => {
            handleResponse(res, err, docs);
        });
    });

    /**
     * Listener
     */
    app.listen(port, () => {
        console.log(`Server is listening on port ${port}`);
    });


    /**
     * Helpers
     */
    function handleResponse(res, err, data) {
        if (err) {
            handleError(res, err);
        } else {
            handleSuccess(res, data);
        }
    }

    function handleError(res, err) {
        let status = 400;
        console.error(err);
        res.status(status);
        res.json([]);
    }

    function handleSuccess(res, data) {
        let status = 200;
        res.status(status);
        res.json(data);
    }

    function getRealMimetype(signature) {
        signature = signature ? signature.toUpperCase() : "";
        if (signature.match(/^FFD8FFE0.*$/) || signature.match(/^FFD8FFE1.*$/) || signature.match(/^FFD8FFDB.*$/)) {
            return "image/jpg";
        } else if (signature.match(/^89504E470D0A1A0A$/)) {
            return "image/png";
        } else if (signature.match(/^474946383761.*$/) || signature.match(/^474946383961.*$/)) {
            return "image/gif";
        } else if (signature.match(/^474946383761.*$/) || signature.match(/^474946383961.*$/)) {
            return "image/tif";
        } else {
            throw new Error("unsupported mimetype");
        }
    }
}());